# Home Assistant Helm Chart

Here's a small helm chart I'm using to play with helm at home. It's slowly being iterated on.

## Includes
- deployment
  - includes configuration file with default allowed trusted reverse proxies, learn more [here](https://www.home-assistant.io/integrations/http#reverse-proxies)
- service account
- service
- ingress
- persistent volumes

## Flow for Helm quick iterations when testing

```bash
# uninstall last installation, don't care what it was. (in my case I'm testing in namespace home-assistant)
helm list -n home-assistant | tail -n 1 | awk '{print $1}' | xargs helm uninstall -n home-assistant

# we sleep because sometimes your machine is slow :shrug:
sleep 2

# install new installation
helm install -n home-assistant --create-namespace --set ingress.host="home-assistant.selfhosting4dogs.com" home-assistant
```

### Notes on Helm I found helpful
- I found this guide by golinuxcloud.com helpful: [Mount ConfigMap as file in existing directory - K8s](https://www.golinuxcloud.com/mount-configmap-as-file-in-existing-directory/#Create_Pod_and_mount_configmap_as_file_in_existing_directory_path)


#### Bonus Docker Testing
This is useful to see is it's even a kubernetes thing, or if what you're trying to do can't work on docker for some reason.

- [Docker Hub page](https://hub.docker.com/r/homeassistant/home-assistant)
- [Linux Installation Guide](https://www.home-assistant.io/installation/linux#install-home-assistant-container)

```bash
# get and run the latest home-assisant stable docker tag with your variables
docker run -d \
  --name homeassistant \
  --privileged \
  --restart=unless-stopped \
  -e TZ=Europe/Amsterdam \
  -v ~/home_asistant:/config \
  --network=host \
  ghcr.io/home-assistant/home-assistant:stable
```
